import { Component, OnInit } from '@angular/core';
import { NavigationService } from '../services/navigation.service';
import { lastValueFrom } from 'rxjs/internal/lastValueFrom';
import { firstValueFrom } from 'rxjs';
import { IModules } from 'src/app/interface/interfaces';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  Modules:IModules[] = [
    { name: "Dashboard", path:"", icon:"fa-house", hasSubMenus: false},
    { name: "Users", path:"/users",icon:"fa-user", hasSubMenus:false  },
    { name: "Quotations", path:"/quotations",icon:"fa-user", hasSubMenus:false  },
    { name: "Quotations Plans", path:"/quotationsplan",icon:"fa-user", hasSubMenus:false  },
    { name: "Customers", path:"/customers",icon:"fa-address-book", hasSubMenus:false  },
    { name: "Catalogs",path:"",icon:"fa-folder-tree",hasSubMenus:true, subMenus:[
      { name: "Types", path:"", icon:"fa-house", hasSubMenus: false},
    ]}
  ];

  constructor(private serviceNavigation: NavigationService) { }

  ngOnInit(): void {
    this.loadModules();
  }

  async loadModules(){
    try {
      const modules$ = this.serviceNavigation.getModules();
      //this.Modules = await firstValueFrom(modules$);
    } catch (error) {
      alert("ERROR");
    }
  }

}
