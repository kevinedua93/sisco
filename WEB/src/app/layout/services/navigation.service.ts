import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';
import { IModules } from 'src/app/interface/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  constructor(private http: HttpClient) { }

  /**
   * Get all modules of system to display on SideBar
   * @returns IModules[]
   */
  getModules(): Observable<IModules[]>{
    return this.http.get<IModules[]>(environment.apiURL+"api/modules").pipe(map( response => response));
  }
}
