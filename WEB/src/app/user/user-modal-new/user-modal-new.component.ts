import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../services/user.service';
import { first, firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-user-modal-new',
  templateUrl: './user-modal-new.component.html',
  styleUrls: ['./user-modal-new.component.scss']
})
export class UserModalNewComponent implements OnInit {
  FormUser: FormGroup;
  FormSubmit:boolean = false;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.FormUser = this.formBuilder.group({
      name:[null,Validators.required],
      lastname:[null,Validators.required],
      email:[null,[Validators.required,Validators.email]],
      active:[true,Validators.required]
    },
    { updateOn:'submit' });
  }

  async saveUser(){
    try {
      let form = this.FormUser;
      form.markAllAsTouched();

      if(form.valid == false){
        alert("Complete all fields requireds");
      }else{
        let d = this.FormUser.value;
        let response = await firstValueFrom(this.userService.saveUser(d));
        this.activeModal.close();
      }
    } catch (error) {
      console.log(error);

    }

  }
}

