import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IUser } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  saveUser(body: IUser): Observable<any>{
    return this.http.post<any>(environment.apiURL+"api/user",body)
  }
  /**
   * Get all customers
   * @returns List customers
   */
  getAll(): Observable<IUser[]>{
    return this.http.get<IUser[]>(environment.apiURL+"api/user");
  }

  getOneByID(id:string): Observable<IUser>{
    return this.http.get<IUser>(environment.apiURL+`api/user/${id}`);
  }
}
