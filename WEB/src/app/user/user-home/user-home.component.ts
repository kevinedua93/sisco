import { Component, OnInit } from '@angular/core';
import { UserModalNewComponent } from '../user-modal-new/user-modal-new.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../services/user.service';
import { firstValueFrom } from 'rxjs';
import { UTable } from 'src/app/global/utilities/u-table';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.scss']
})
export class UserHomeComponent implements OnInit {
  public TableUsers:UTable = new UTable();

  constructor(
    private modalService: NgbModal,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.updateTable();
  }

  async updateTable(){
    try {
      let response = await firstValueFrom(this.userService.getAll());
      this.TableUsers.setData(response);
    } catch (error) {

    }
  }

  modalNewUser(){
    let modal = this.modalService.open(UserModalNewComponent,{ size:"md" });
    modal.result.then(close => this.updateTable());
  }

}
