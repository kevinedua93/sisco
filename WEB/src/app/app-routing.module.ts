import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'users', loadChildren: () => import("./user/user.module").then(m => m.UserModule) },
  { path: 'quotations', loadChildren: () => import("./quotation/quotation.module").then(m => m.InvoiceModule) },
  { path: 'quotationsplan', loadChildren: () => import("./quotationplan/quotationplan.module").then(m => m.QuotationplanModule) },
  { path: 'customers', loadChildren: () => import("./customer/customer.module").then(m => m.CustomerModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
