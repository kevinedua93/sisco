import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ICustomer } from '../interfaces/customer';
import { CustomerService } from '../services/customer.service';
import { firstValueFrom } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

export class Customer{
  public Name:string;

  public constructor(init?: Partial<Customer>) {
    Object.assign(this, init);
  }
}

@Component({
  selector: 'app-customer-modal-new',
  templateUrl: './customer-modal-new.component.html',
  styleUrls: ['./customer-modal-new.component.scss']
})
export class CustomerModalNewComponent implements OnInit {
  public CustomerID:string = "";
  private Customer: ICustomer;
  CustomerForm: FormGroup;

	constructor(
    public activeModal: NgbActiveModal,
    private SCustomer: CustomerService,
    private fb: FormBuilder
    ) {}

  ngOnInit(): void {
    this.CustomerForm = this.fb.group({
      name: [null,[Validators.required]],
      active: [true,[Validators.required]],
      type: [null,[Validators.required]],
      address: [null,[Validators.required]],
    });
  }

  async loadData(){
    //this.Customer = await firstValueFrom(this.SCustomer.getCustomerByID(this.CustomerID));
    //this.CustomerForm.patchValue(this.Customer);
  }

  SubmitCustomerForm(){
    this.CustomerForm.updateValueAndValidity();
    this.Customer = this.CustomerForm.value;
    console.log(this.Customer);

  }
}
