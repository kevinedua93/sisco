import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerModalNewComponent } from './customer-modal-new.component';

describe('CustomerModalNewComponent', () => {
  let component: CustomerModalNewComponent;
  let fixture: ComponentFixture<CustomerModalNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerModalNewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomerModalNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
