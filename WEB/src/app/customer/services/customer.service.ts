import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ICustomer } from '../interfaces/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) { }

  /**
   * Get all customers
   * @returns List customers
   */
  getCustomers(): Observable<ICustomer[]>{
    return this.http.get<ICustomer[]>(environment.apiURL+"api/customer");
  }

  getCustomerByID(id:string): Observable<ICustomer>{
    return this.http.get<ICustomer>(environment.apiURL+`api/customer/${id}`);
  }
}
