export interface ICustomer {
  _id:     string;
  name:    string;
  type:    string;
  active:  boolean;
}
