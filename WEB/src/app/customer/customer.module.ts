import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerHomeComponent } from './customer-home/customer-home.component';
import { GlobalModule } from '../global/global.module';
import { CustomerModalNewComponent } from './customer-modal-new/customer-modal-new.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomerProfileComponent } from './customer-profile/customer-profile.component';


@NgModule({
  declarations: [
    CustomerHomeComponent,
    CustomerModalNewComponent,
    CustomerProfileComponent
  ],
  imports: [
    GlobalModule,
    CommonModule,
    CustomerRoutingModule,
    ReactiveFormsModule
  ]
})
export class CustomerModule { }
