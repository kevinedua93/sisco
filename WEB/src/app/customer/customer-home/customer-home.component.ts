import { Component, ElementRef, HostListener, OnInit } from '@angular/core';
import { CustomerService } from '../services/customer.service';
import { ICustomer } from '../interfaces/customer';
import { UTable } from "src/app/global/utilities/u-table";
import { first, firstValueFrom, take } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomerModalNewComponent } from '../customer-modal-new/customer-modal-new.component';

@Component({
  selector: 'app-customer-home',
  templateUrl: './customer-home.component.html',
  styleUrls: ['./customer-home.component.scss']
})

export class CustomerHomeComponent implements OnInit {

  TableCustomers:UTable = new UTable();

  constructor(
    private modalService: NgbModal,
    private SCustomer: CustomerService
  ) { }

  ngOnInit(): void {
    this.loadCustomer();
  }

  async loadCustomer(){
    let customers = await firstValueFrom(this.SCustomer.getCustomers());
    this.TableCustomers.setData(customers);
  }

  modalNew(){
    let modal = this.modalService.open(CustomerModalNewComponent,{ size:"md" });
    modal.componentInstance.CustomerID = "64b8961353bbabe80cc67ab5";
    modal.result.then(
      success =>{
        console.log(success);
        this.loadCustomer();
      },
      reason =>{
        console.log(reason);

      }
    );
  }

  profile(){

  }
}
