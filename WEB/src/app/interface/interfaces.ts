export interface IModules {
  name: string,
  path: string,
  icon: string,
  hasSubMenus:boolean,
  subMenus?:IModules[]
};
