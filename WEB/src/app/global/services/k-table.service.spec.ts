import { TestBed } from '@angular/core/testing';

import { KTableService } from './k-table.service';

describe('KTableService', () => {
  let service: KTableService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KTableService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
