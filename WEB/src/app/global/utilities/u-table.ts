
interface Pages{ number: number, active: boolean }

/**
 * @author Kevin Eduardo Cerda Salazar
 */
export class UTable{
  public CurrentPage: number = 1;
  public Size: number = 10;

  public Pages: Pages[] = [];
  public RowStart: number = 0;
  public RowEnd: number = 10;
  public NumRows: number = 0;


  public SortAsc:boolean = true;

  private PagesTotal:number = 0;

  /**
   * Number of pages on pagination show.
   * @description this number limited how many show N pages to the left and rigth with the current page.
   * @example
   * ShowPages = 2 <= Default value;
   * [1] [2] [CURRENT] [4] [5]
   */
  public ShowPages: number = 2;
  get ShowFirstPage( ){ return this.PaginationStart != 1 }
  get ShowLastPage( ){ return this.PaginationLimit != this.PagesTotal }

  get PaginationStart( ){ return this._PaginationStart; }
  get PaginationLimit( ){ return this._PaginationLimit; }

  private _PaginationStart: number = 1;
  private _PaginationLimit: number = 5;

  public Data:any[] = [];
  private _Data:any[]= [];
  private _DataCalculate:any[] = [];

  get RowsQuantity(){ return this.Data.length; }
  get NextPageDisable(){ return this.CurrentPage >= this.PagesTotal; }
  get PrevPageDisable(){ return this.CurrentPage <= 1; }

  constructor(){}


  /**
   * Set data to KTABLE
   * @param data JSON data
   */
  setData(data:any){
    this._Data = data;
    this.Data = data;
    this._DataCalculate = data;
    this.calculate();
  }

  /**
   * Move to next page in table.
   */
  nextPage(){
    if(!this.NextPageDisable){
      this.CurrentPage += 1;
      this.RowStart += this.Size;
      this.RowEnd += this.Size;
    }
    this.movePage();
  }

  /**
   * Move to previous page in table.
   */
  prevPage(){
    if(!this.PrevPageDisable){
      this.CurrentPage -= 1;
      this.RowStart -= this.Size;
      this.RowEnd -= this.Size
    }
    this.movePage();
  }

  /**
   * Move to specific page
   * @param page Number of page
   */
  setPage(page:number){
    this.CurrentPage = page;
    this.calculate();
  }

  private movePage(){
    this.generatePages();
    this.Data = this._DataCalculate.slice(this.RowStart,this.RowEnd);
  }

  /**
   * Calculate pages,rows,disabled buttons next,prev page.
   */
  private calculate(){
    this.NumRows = this._DataCalculate.length;
    this.RowStart = this.NumRows > 0 ? ( this.CurrentPage * this.Size ) - this.Size : 0;
    this.RowEnd = this.NumRows > 0 ? this.CurrentPage * this.Size : 0;
    this.PagesTotal = Math.ceil(this._DataCalculate.length / this.Size);

    this.generatePages();

    this.Data = this._DataCalculate.slice(this.RowStart,this.RowEnd);
  }

  /**
   * Calculate and generate number of pages
   */
  private generatePages(){
    this.Pages = [];
    let nextEnd = this.CurrentPage + this.ShowPages;
    let nextStart = this.CurrentPage - this.ShowPages;

    this._PaginationStart  = nextStart >= 1 ? nextStart : 1 ;
    this._PaginationLimit = nextEnd >= this.PagesTotal ? this.PagesTotal : nextEnd;

    for(let i = this._PaginationStart; i <= this._PaginationLimit; i++){
      let page:Pages = { number: i, active: i == this.CurrentPage};
      this.Pages.push(page);
    }
  }

  /**
   * Sort by name of property.
   * @description Automatic Toggle property "SortAsc" on each execute to order by asc or desc.
   * @param property Name of property in json data
   * @example function Sort("name");
   *
   * Data = [ { name:"A" , lastname: "B"} ];
   */
  Sort(property:string) {
    this._DataCalculate.sort((a:any, b:any) =>{
      if(this.SortAsc){
        return a[property] >= b[property] ? 1 : -1
      }else{
        return a[property] <= b[property] ? 1 : -1
      }
    });
    this.SortAsc = !this.SortAsc;
    this.calculate();
  }

  /**
   * Search text on all values of propertys of data json
   * @param text String to search in data json
   */
  search(text:any){
    let keys = Object.keys(this._Data[0]);

    this._DataCalculate = this._Data.filter(item => keys.find( (key) => (typeof item[key] == 'string') ? item[key].toLowerCase().includes(text):false ));

    this.calculate();
  }

  /**
   * Change the size of page.
   * @param size Numer of rows per page.
   */
  changeSizePage(size:number){
    this.Size = size;
    this.calculate();
  }
}
