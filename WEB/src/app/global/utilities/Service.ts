export class Service{
  public name:string;
  public description:string;
  public price:number = 0;
  public quantity:number = 0;
  public total:number = 0;

  calculateTotal(){
    this.total = this.price * this.quantity;
  }
}
