import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KTableService } from './services/k-table.service';

@NgModule({
  declarations: [
  ],
  exports:[
  ],
  imports: [
    CommonModule
  ],
  providers:[
    KTableService
  ]
})
export class GlobalModule { }
