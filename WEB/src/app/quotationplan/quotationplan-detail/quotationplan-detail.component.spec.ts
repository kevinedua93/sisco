import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotationplanDetailComponent } from './quotationplan-detail.component';

describe('QuotationplanDetailComponent', () => {
  let component: QuotationplanDetailComponent;
  let fixture: ComponentFixture<QuotationplanDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuotationplanDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuotationplanDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
