import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuotationplanHomeComponent } from './quotationplan-home/quotationplan-home.component';
import { QuotationplanDetailComponent } from './quotationplan-detail/quotationplan-detail.component';

const routes: Routes = [
  { path: '', component: QuotationplanHomeComponent },
  { path: 'detail/:id', component: QuotationplanDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuotationplanRoutingModule { }
