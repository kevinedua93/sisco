import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotationplanHomeComponent } from './quotationplan-home.component';

describe('QuotationplanHomeComponent', () => {
  let component: QuotationplanHomeComponent;
  let fixture: ComponentFixture<QuotationplanHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuotationplanHomeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuotationplanHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
