import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { firstValueFrom } from 'rxjs';
import { UTable } from 'src/app/global/utilities/u-table';
import { InvoiceService } from 'src/app/quotation/services/invoice.service';

@Component({
  selector: 'app-quotationplan-home',
  templateUrl: './quotationplan-home.component.html',
  styleUrls: ['./quotationplan-home.component.scss']
})
export class QuotationplanHomeComponent implements OnInit {
  public TableInvoices:UTable = new UTable();

  constructor(
    private modalService: NgbModal,
    public invoiceService: InvoiceService
  ) { }

  ngOnInit(): void {

    this.updateTable();
  }

  async updateTable(){
    try {
      let response = await firstValueFrom(this.invoiceService.getTable());
      this.TableInvoices.setData(response);
      console.log(this.TableInvoices);

    } catch (error) {

    }
  }
}
