import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuotationplanRoutingModule } from './quotationplan-routing.module';
import { QuotationplanHomeComponent } from './quotationplan-home/quotationplan-home.component';
import { QuotationplanDetailComponent } from './quotationplan-detail/quotationplan-detail.component';


@NgModule({
  declarations: [
    QuotationplanHomeComponent,
    QuotationplanDetailComponent
  ],
  imports: [
    CommonModule,
    QuotationplanRoutingModule
  ]
})
export class QuotationplanModule { }
