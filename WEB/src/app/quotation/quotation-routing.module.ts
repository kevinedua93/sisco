import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { quotationHomeComponent } from './quotation-home/quotation-home.component';
import { QuotationDetailComponent } from './quotation-detail/quotation-detail.component';

const routes: Routes = [
  { path: '', component: quotationHomeComponent },
  { path: 'detail/:id', component: QuotationDetailComponent},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceRoutingModule { }
