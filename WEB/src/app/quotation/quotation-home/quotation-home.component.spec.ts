import { ComponentFixture, TestBed } from '@angular/core/testing';

import { quotationHomeComponent } from './quotation-home.component';

describe('quotationHomeComponent', () => {
  let component: quotationHomeComponent;
  let fixture: ComponentFixture<quotationHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ quotationHomeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(quotationHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
