import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UTable } from 'src/app/global/utilities/u-table';
import { InvoiceModalNewComponent } from '../invoice-modal-new/invoice-modal-new.component';
import { InvoiceService } from '../services/invoice.service';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-quotation-home',
  templateUrl: './quotation-home.component.html',
  styleUrls: ['./quotation-home.component.scss']
})
export class quotationHomeComponent implements OnInit {

  public TableInvoices:UTable = new UTable();

  constructor(
    private modalService: NgbModal,
    public invoiceService: InvoiceService
  ) { }

  ngOnInit(): void {

    this.updateTable();
  }

  async updateTable(){
    try {
      let response = await firstValueFrom(this.invoiceService.getTable());
      this.TableInvoices.setData(response);
      console.log(this.TableInvoices);

    } catch (error) {

    }
  }

  modalNewInvoice(){
    let modal = this.modalService.open(InvoiceModalNewComponent,{size:"xl", fullscreen:false, backdrop:"static", keyboard:false })
    .result.then(
      close => { this.updateTable() },
      dismis => { });
  }

}
