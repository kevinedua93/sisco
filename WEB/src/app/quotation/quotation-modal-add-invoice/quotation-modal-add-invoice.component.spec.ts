import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotationModalAddInvoiceComponent } from './quotation-modal-add-invoice.component';

describe('QuotationModalAddInvoiceComponent', () => {
  let component: QuotationModalAddInvoiceComponent;
  let fixture: ComponentFixture<QuotationModalAddInvoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuotationModalAddInvoiceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuotationModalAddInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
