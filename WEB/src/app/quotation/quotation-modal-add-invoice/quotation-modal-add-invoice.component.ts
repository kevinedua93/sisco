import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { InvoiceService } from '../services/invoice.service';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-quotation-modal-add-invoice',
  templateUrl: './quotation-modal-add-invoice.component.html',
  styleUrls: ['./quotation-modal-add-invoice.component.scss']
})
export class QuotationModalAddInvoiceComponent implements OnInit {
  XMLdata:any = null;
  QuotationSelected:string;
  FormInvoice: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    public invoiceService: InvoiceService
  ) { }

  ngOnInit(): void {
    this.FormInvoice = this.formBuilder.group({
      quotation:[this.QuotationSelected,localStorage,Validators.required],
      filexml: [null,Validators.required],
      filepdf: [null,Validators.required],
      datexml:[null,Validators.required],
      subtotalxml:[null,Validators.required],
      taxtrasxml:[null,Validators.required],
      taxretxml:[null,Validators.required],
      totalxml:[null,Validators.required],
      comments: [null,Validators.required],
    },
    { updateOn:'submit' });
  }

  changefileXML(event:any){
    const file = event.target.files[0];
        if (!file) {
            return;
        }
    const reader = new FileReader();
    reader.readAsText(file);
    reader.onload = (evt) => {
      const xmlString: string = (evt as any).target.result;
      var convert = require('xml-js');
      var result1 = convert.xml2json(xmlString, {compact: false, spaces: 4});
      this.XMLdata = JSON.parse(result1);
      this.readXMLdata();
    };
  }

  readXMLdata(){
    console.log(this.XMLdata);
    this.FormInvoice.patchValue({
      datexml: this.XMLdata.elements[0].attributes.Fecha,
      subtotalxml: this.XMLdata.elements[0].attributes.SubTotal,
      taxtrasxml: this.XMLdata.elements[0].elements[3].attributes.TotalImpuestosTrasladados,
      taxretxml: this.XMLdata.elements[0].elements[3].attributes.TotalImpuestosRetenidos,
      totalxml: this.XMLdata.elements[0].attributes.Total
    });
  }


  changePDF(event: Event){
    const input = (event.target as HTMLInputElement);
    if(input.files && input.files.length > 0) {
      this.FormInvoice.patchValue({ filepdf: input.files[0]});
    }
  }

  changeXML(event: Event){
    const input = (event.target as HTMLInputElement);
    if(input.files && input.files.length > 0) {
      this.FormInvoice.patchValue({ filexml: input.files[0]});
    }
  }

  async saveInvoice(){
    try {
      console.log(this.FormInvoice.value);

      if(this.FormInvoice.valid){
        let data = this.FormInvoice.value;
        const formData: FormData = new FormData();
        formData.append('filexml', data['filexml']);
        formData.append('filepdf', data['filepdf']);
        formData.append('comments', data['comments']);
        formData.append('datexml', data['datexml']);
        formData.append('subtotalxml', data['subtotalxml']);
        formData.append('taxtrasxml', data['taxtrasxml']);
        formData.append('taxretxml', data['taxretxml']);
        formData.append('totalxml', data['totalxml']);
        formData.append('quotation', data['quotation']);

        let response = await firstValueFrom(this.invoiceService.saveInvoice(formData));
        this.activeModal.close();
      }else{
        alert("Completa formulario");
      }
    } catch (error) {
      alert("error");
    }
  }
}
