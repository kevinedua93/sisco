import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvoiceRoutingModule } from './quotation-routing.module';
import { quotationHomeComponent } from './quotation-home/quotation-home.component';
import { InvoiceModalNewComponent } from './invoice-modal-new/invoice-modal-new.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuotationDetailComponent } from './quotation-detail/quotation-detail.component';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { QuotationModalAddServiceComponent } from './quotation-modal-add-service/quotation-modal-add-service.component';
import { QuotationModalAddPaymentComponent } from './quotation-modal-add-payment/quotation-modal-add-payment.component';
import { QuotationModalAddInvoiceComponent } from './quotation-modal-add-invoice/quotation-modal-add-invoice.component';


@NgModule({
  declarations: [
    quotationHomeComponent,
    InvoiceModalNewComponent,
    QuotationDetailComponent,
    QuotationModalAddServiceComponent,
    QuotationModalAddPaymentComponent,
    QuotationModalAddInvoiceComponent
  ],
  imports: [
    CommonModule,
    InvoiceRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgbCollapseModule
  ]
})
export class InvoiceModule { }
