import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IInvoice } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(private http: HttpClient) { }

  getDetail(id:any): Observable<any>{
    return this.http.get<any>(environment.apiURL+"api/invoice/"+id)
  }

  getTable(): Observable<any>{
    return this.http.get<any>(environment.apiURL+"api/invoice")
  }

  saveQuotation(body: IInvoice): Observable<any>{
    return this.http.post<any>(environment.apiURL+"api/invoice",body)
  }

  savePayment(body:any):Observable<any>{
    return this.http.post<any>(environment.apiURL+"api/quotation/payment",body)
  }

  saveInvoice(body:FormData){
    return this.http.post<any>(environment.apiURL+"api/quotation/invoice",body,{ reportProgress: true, responseType: 'json'});
  }

}
