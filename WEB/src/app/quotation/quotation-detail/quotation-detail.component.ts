import { Component, OnInit } from '@angular/core';
import { InvoiceService } from '../services/invoice.service';
import { firstValueFrom } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { UTable } from 'src/app/global/utilities/u-table';
import { QuotationModalAddServiceComponent } from '../quotation-modal-add-service/quotation-modal-add-service.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { QuotationModalAddPaymentComponent } from '../quotation-modal-add-payment/quotation-modal-add-payment.component';
import { QuotationModalAddInvoiceComponent } from '../quotation-modal-add-invoice/quotation-modal-add-invoice.component';

@Component({
  selector: 'app-quotation-detail',
  templateUrl: './quotation-detail.component.html',
  styleUrls: ['./quotation-detail.component.scss']
})
export class QuotationDetailComponent implements OnInit {
  public TableServices:UTable = new UTable();
  public TablePayments:UTable = new UTable();
  public TableInvoices:UTable = new UTable();
  private _quotationSelected:string;

  Quotation:any = null;
  isCollapsed = false;

  PAIDPERCENT:number = 0;

  constructor(
    private modalService: NgbModal,
    private route: ActivatedRoute,
    public invoiceService: InvoiceService
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  modalAddService(){
    let modal = this.modalService.open(QuotationModalAddServiceComponent,{size:"xl", fullscreen:false, backdrop:"static", keyboard:false })
    .result.then(
      close => { },
      dismis => { });
  }

  modalAddPayment(){
    let modal = this.modalService.open(QuotationModalAddPaymentComponent,{size:"lg", fullscreen:false, backdrop:"static", keyboard:false });
    modal.componentInstance.QuotationSelected = this._quotationSelected;
    modal.result.then(
      close => { },
      dismis => { });
  }

  modalAddInvoice(){
    let modal = this.modalService.open(QuotationModalAddInvoiceComponent,{size:"lg", fullscreen:false, backdrop:"static", keyboard:false });
    modal.componentInstance.QuotationSelected = this._quotationSelected;
    modal.result.then(
      close => { },
      dismis => { });
  }

  async getData(){
    try {
      let params = await firstValueFrom(this.route.params);
      this._quotationSelected = params['id'];
      this.Quotation = await firstValueFrom(this.invoiceService.getDetail(this._quotationSelected));

      this.TableServices.Size = 9999;
      this.TableServices.setData(this.Quotation.services);
      this.TablePayments.setData(this.Quotation.payments);
      this.TableInvoices.setData(this.Quotation.invoices);

      let paidPercent = (this.Quotation.paid / this.Quotation.total) * 100;
      this.PAIDPERCENT = parseInt(paidPercent.toFixed());

      console.log(this.Quotation);



    } catch (error) {

    }
  }
}
