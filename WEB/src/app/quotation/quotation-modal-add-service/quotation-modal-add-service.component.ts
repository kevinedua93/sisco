import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { InvoiceService } from '../services/invoice.service';
import { Service } from 'src/app/global/utilities/Service';
import { UTable } from 'src/app/global/utilities/u-table';

@Component({
  selector: 'app-quotation-modal-add-service',
  templateUrl: './quotation-modal-add-service.component.html',
  styleUrls: ['./quotation-modal-add-service.component.scss']
})
export class QuotationModalAddServiceComponent implements OnInit {
  Service = new Service();
  ServicesList:Service[] = [];
  ServicesTable = new UTable();

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    public invoiceService: InvoiceService
  ) { }

  ngOnInit(): void {
  }

  addService(){
    this.ServicesList.push(this.Service);
    this.ServicesTable.setData(this.ServicesList);
    this.Service = new Service();
  }

}
