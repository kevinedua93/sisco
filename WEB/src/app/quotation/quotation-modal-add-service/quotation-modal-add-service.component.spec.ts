import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotationModalAddServiceComponent } from './quotation-modal-add-service.component';

describe('QuotationModalAddServiceComponent', () => {
  let component: QuotationModalAddServiceComponent;
  let fixture: ComponentFixture<QuotationModalAddServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuotationModalAddServiceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuotationModalAddServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
