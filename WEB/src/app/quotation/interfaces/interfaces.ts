export interface IInvoice {
  customer: string,
  services: Array<any>,
  active: boolean
};
