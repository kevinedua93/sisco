import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { InvoiceService } from '../services/invoice.service';
import { firstValueFrom } from 'rxjs';
import { Service } from 'src/app/global/utilities/Service';

class quotation {
  public customer:string;
  public services:Service[] = [];
  public taxUnit:number = 0;
  public isrRetUnit: number = 1.25;

  public subtotal: number = 0;
  public tax:number = 0;
  public total: number = 0;
  public isrRet: number = 0;

  public tasaAplicable:number = 0;
  private tasaAplicable_Resico_2023 = [
    {value: 25000, percent: 1},
    {value: 50000, percent: 1.10},
    {value: 83333.33, percent: 1.50},
    {value: 208333.33, percent: 2},
    {value: 3500000, percent: 2.50},
  ];
  /**
   * Calculate the total of invoice
   */
  calculateValues():void{
    //Set in 0 total
    this.subtotal = 0;
    this.tax = 0;
    this.total = 0;

    //Calculate the new values
    this.services.forEach(item => this.subtotal += item.total );
    this.tax = (this.subtotal * this.taxUnit) / 100;
    this.isrRet = (this.subtotal * this.isrRetUnit) / 100;

    this.total = this.subtotal + this.tax;

    //Validate if aply this tax
    this.total -= this.isrRet;

    //Validate if aply this tax
    this.calculateTasaAplicable();
  }

  calculateTasaAplicable(){
    let start = 0;
    this.tasaAplicable_Resico_2023.forEach(item =>{
      if(this.subtotal > start && this.subtotal <= item.value){
        console.log(item.percent);
      }

      start = item.value;
    });
  }

  /**
   * Add a new service on invoice and update total
   * @param Service New service
   * @description Add a new service on the invoice and update the total.
   */
  addService(Service:Service){
    this.services.push(Service);
    this.calculateValues();
  }

  /**
   * Delete a service
   * @param Service service on invoice
   * @description Delete a service on the invoice and update the total.
   */
  deleteService(Service: Service){
    this.services = this.services.filter(x => x.name != Service.name);
    this.calculateValues();
  }
}

@Component({
  selector: 'app-invoice-modal-new',
  templateUrl: './invoice-modal-new.component.html',
  styleUrls: ['./invoice-modal-new.component.scss']
})
export class InvoiceModalNewComponent implements OnInit {
  FormQuotation: FormGroup;
  ServiceSelected = new Service();

  QUOTATION = new quotation ();

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    public invoiceService: InvoiceService
  ) { }

  ngOnInit(): void {
    this.FormQuotation = this.formBuilder.group({
      customer:[null,Validators.required],
      services: [[],Validators.required],
      subtotal: [0,Validators.required],
      taxUnit: [0,Validators.required],
      tax: [0,Validators.required],
      isrRetUnit: [0,Validators.required],
      isrRet: [0,Validators.required],
      total: [0,Validators.required]
    },
    { updateOn:'submit' });
  }

  addService(){
    this.QUOTATION.addService(this.ServiceSelected);
    this.ServiceSelected = new Service();
    this.FormQuotation.patchValue(this.QUOTATION);
  }

  deleteService(Service: Service){
    this.QUOTATION.deleteService(Service);
    this.FormQuotation.patchValue(this.QUOTATION);
  }

  editService(Service: Service){
    this.QUOTATION.deleteService(Service);
    this.ServiceSelected = Service;
    this.FormQuotation.patchValue(this.QUOTATION);
  }

  setTaxUnit(Event:any){
    this.QUOTATION.calculateValues();
    this.FormQuotation.patchValue(this.QUOTATION);
  }

  async saveQuotation(){
    try {
      if(this.FormQuotation.valid){
        let data = this.FormQuotation.value;
        let response = await firstValueFrom(this.invoiceService.saveQuotation(data));
        this.activeModal.close();
      }
    } catch (error) {
      alert("ERROR");
    }
  }

}
