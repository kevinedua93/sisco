import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceModalNewComponent } from './invoice-modal-new.component';

describe('InvoiceModalNewComponent', () => {
  let component: InvoiceModalNewComponent;
  let fixture: ComponentFixture<InvoiceModalNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoiceModalNewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InvoiceModalNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
