import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { firstValueFrom } from 'rxjs';
import { InvoiceService } from '../services/invoice.service';

@Component({
  selector: 'app-quotation-modal-add-payment',
  templateUrl: './quotation-modal-add-payment.component.html',
  styleUrls: ['./quotation-modal-add-payment.component.scss']
})
export class QuotationModalAddPaymentComponent implements OnInit {

  QuotationSelected:string;
  FormPayment: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    public invoiceService: InvoiceService
  ) { }

  ngOnInit(): void {
    this.FormPayment = this.formBuilder.group({
      quotation:[this.QuotationSelected,localStorage,Validators.required],
      datepayment:[null,Validators.required],
      value: [null,Validators.required],
      reference:[null,Validators.required]
    },
    { updateOn:'submit' });
  }

  async savePayment(){
    try {
      if(this.FormPayment.valid){
        let data = this.FormPayment.value;
        let response = await firstValueFrom(this.invoiceService.savePayment(data));
        this.activeModal.close();
      }else{
        alert("FORM INVALID");
      }
    } catch (error) {
      alert("ERROR");
    }
  }

}
