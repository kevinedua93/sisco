import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuotationModalAddPaymentComponent } from './quotation-modal-add-payment.component';

describe('QuotationModalAddPaymentComponent', () => {
  let component: QuotationModalAddPaymentComponent;
  let fixture: ComponentFixture<QuotationModalAddPaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuotationModalAddPaymentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuotationModalAddPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
