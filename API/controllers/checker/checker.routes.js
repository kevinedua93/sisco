'use strict';
const express = require('express');
var router = express.Router();
const { employeesController, checkerController } = require('./checker.controller');
const { employeesModels, CheckerModels } = require('./checker.models');
const { expressValidatorCustom } = require('../../helpers/customExpressValidator');

router.post('/checker', CheckerModels.create,expressValidatorCustom.validateRequest,checkerController.create);

module.exports = router;