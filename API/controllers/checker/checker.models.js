const {query, body, param} = require('express-validator');
const { expressValidatorCustom } = require('../../helpers/customExpressValidator')

var CheckerModels = {
    create:[
        //body("employee").notEmpty().isMongoId().customSanitizer(expressValidatorCustom.toMongoId),
        body("location").notEmpty(),
        body("date").customSanitizer( value => new Date()),
        body("status").notEmpty().isString().isIn(['OK','LATE'])
    ],
}
module.exports =  { CheckerModels };