'use strict'
const { clientDB } = require('../../helpers/mongodb');
const tableName = "quotation";
const fs = require('fs');

/**
 * Calculate total of quotation
 * @param {Quotation} quotation services of quotation 
 * @returns quotation {Object}
 */
async function QuotationGetNewBalance(quotation){
  try {
    let tasaAplicable_Resico_2023 = [
      {value: 25000, percent: 1},
      {value: 50000, percent: 1.10},
      {value: 83333.33, percent: 1.50},
      {value: 208333.33, percent: 2},
      {value: 3500000, percent: 2.50},
    ];

    let subtotal = 0;
    quotation.services.forEach(item => { subtotal += item.total; });

    quotation.subtotal = subtotal;
    quotation.tax = (quotation.subtotal * quotation.taxUnit)/100;
    quotation.total = quotation.subtotal + quotation.tax;

    //Valid if aply
    quotation.isrRet = (quotation.subtotal * quotation.isrRetUnit)/100;
    quotation.total -= quotation.isrRet;

    quotation.balance = quotation.total - quotation.paid;
    return quotation;
  } catch (error) {
    throw new Error(error);
  }
}

const Controller = {
  create: async (req,res)=>{
      try{
          let response = {status:"OK",message:null ,data: req.body};
          let data = await clientDB.db("sisco").collection(tableName).insertOne(req.body);
          response.data = data;
          response.message = "Create succesfully";
          res.status(200).json(response);
      }catch(error){
          throw new Error(error);
      }
  },
  addPay: async (req,res)=>{
    try{
      let response = {status:"OK",message:null ,data: req.body};

      let data = await clientDB.db("sisco").collection(tableName).aggregate([
        { $match:{ "_id": req.body.quotation}},
        { $project: { 
          total:"$total",
          balance:"$balance",
          paidSum:{$sum:"$payments.value"}
        } }
        ]).next();    

      let balance = data.balance - req.body.value;
      let paid = data.paidSum + req.body.value;

      await clientDB.db("sisco").collection(tableName).updateOne(
        {"_id": req.body.quotation},
        {
          $set:{ 
            balance: balance,
            paid: paid },
          $push:{ 
            payments: { 
              date: req.body.date, 
              value: req.body.value,
              reference: req.body.reference
            } }
        } );

      response.message = "Create succesfully";
      res.status(200).json(response);
    }catch(error){
        throw new Error(error);
    }
  },
  detDetailByID: async(req,res)=>{
      try {
          let response = {status:"OK", message:null ,data: null};
          let data = await clientDB.db("sisco").collection(tableName).aggregate([
              { $match:{ "_id": req.params.id}},
              // { $project: {
              //     name:"$name",
              //     type:"$type",
              //     active:"$active"
              // } }
          ]).next();

          response.message = "Get data succesfully";
          res.status(200).json(data);
      } catch (error) {
          throw new Error(error);
      }
  },
  getAll: async(req,res)=>{
      try {
          let response = {status:"OK", message:null ,data: null};
          let data = await clientDB.db("sisco").collection(tableName).aggregate([
              {
                  $project:{
                      customer:"$customer",
                      date:{ $dateToString: { date: "$date", format: "%Y-%m-%d"} },
                      total:"$total",
                      balance:"$balance",
                      paid:"$paid"
                  }
              }
          ]).toArray();
          response.message = "Get data succesfully";
          res.status(200).json(data);
      } catch (error) {
          throw new Error(error);
      }
  },
  addService: async (req,res)=>{
    try{
      let response = {status:"OK",message:null ,data: req.body};

      await clientDB.db("sisco").collection(tableName).updateOne(
        {"_id": req.body.id},
        {
          $push:{ 
            services: {
              id: req.body.id,
              date: req.body.date,
              name: req.body.name,
              price: req.body.price,
              quantity: req.body.quantity,
              total: req.body.total
            } }
        } );

      /**
       * Calculate and update total of quotation
       */
      let quotationData = await clientDB.db("sisco").collection(tableName).aggregate([ { $match:{ "_id": req.body.quotation}}]).next();
      let quotation = await QuotationGetNewBalance(quotationData);
      await clientDB.db("sisco").collection(tableName).updateOne(
        {"_id": req.body.quotation}, 
        { $set: {
          subtotal: quotation.subtotal,
          total : quotation.total,
          tax: quotation.tax,
          isrRet: quotation.isrRet,
          balance: quotation.balance
        } } );

      response.message = "Create succesfully";
      res.status(200).json(response);
    }catch(error){
        throw new Error(error);
    }
  },
  /**
   * Add invoice to quotation
   * @param {*} req 
   * @param {*} res 
   */
  addInvoice: async(req,res)=>{
    try {
      let response = {status:"OK",message:null};
      
      await clientDB.db("sisco").collection(tableName).updateOne(
        {"_id": req.body.quotation},
        {
          $push:{ invoices: { 
            id: req.body.id,
            dateCreate: req.body.dateCreate,
            date:req.body.datexml,
            subtotal: req.body.subtotalxml,
            taxtras: req.body.taxtrasxml,
            taxret: req.body.taxretxml,
            total: req.body.totalxml,
            comments: req.body.comments,
            paid: false,
            filePDF: req.files.filepdf[0],
            fileXML: req.files.filexml[0]
          } }
        } );

      response.message = "Create succesfully";
      res.status(200).json(response);
    } catch (error) {
      throw new Error(error);
    }
  },

  createPlantAutomatic: async (req,res)=>{
    try {
      let response = {status:"OK", message:null ,data: null};
      let services = await clientDB.db("sisco").collection(tableName).aggregate([
          { $match:{ "_id": req.params.id}},
          { $project: { services:"$services" } },
          { $unwind: "$services" },
          { $project: { _id:0, id:"$services.id", name:"$services.name" } }
      ])

      let data = [];
      for await(const item of services){
        data.push({
          quotationid: req.params.id,
          serviceid: item.id,
          name:item.name,
          createDate: new Date(),
          dateDue:"",
          completeDate:"",
          complete:false,
        });
      }

      await clientDB.db("sisco").collection("quotationplan").insertMany(data);

      response.message = "create quotation plan succesfully";
      res.status(200).json(response);
    } catch (error) {
      throw new Error(error);
    }
  },
  getInvoicePDF: async (req,res)=>{
    try {
      let response = {status:"OK", message:null ,data: null};
      let data = await clientDB.db("sisco").collection(tableName).aggregate([
        { $match:{ "_id": req.params.id}},
        // { $project: {
        //     name:"$name",
        //     type:"$type",
        //     active:"$active"
        // } }
      ]).next();
      
      // res.contentType(data.invoices[0].filePDF.mimetype);
      res.set(`Content-disposition', 'attachment; filename="${data.invoices[0].filePDF.originalname}"`);
      res.set('Content-type', data.invoices[0].filePDF.mimetype);
      // res.send(data.invoices[0].filePDF.buffer);
      // response.message = "Get data succesfully";
      // res.status(200).json(data.invoices[0].filePDF.buffer);

      res.status(200).send(new Buffer(data.invoices[0].filePDF.buffer,data.invoices[0].encoding));

    //  data =  fs.readFileSync(data.invoices[0].filePDF.buffer)
    //   res.contentType("application/pdf");
    //   res.send(data);

    } catch (error) {
      throw new Error(error);

    }
    
  }
};

module.exports = { invoiceController: Controller };
