const {query, body, param} = require('express-validator');
const { expressValidatorCustom } = require('../../helpers/customExpressValidator')

var Model = {
  create:[
    body("date").customSanitizer( value => new Date()),
    body("customer").notEmpty().isString(),
    body("services").notEmpty().isArray(),
    body("services.*.id").customSanitizer(expressValidatorCustom.newMongoId),
    body("payments").customSanitizer( value => []),
    body("subtotal").notEmpty(),
    body("taxUnit").notEmpty(),
    body("tax").notEmpty(),
    body("isrRetUnit").notEmpty(),
    body("isrRet").notEmpty(),
    body("total").notEmpty(),
    body("balance").customSanitizer( (value, { req })  =>  req.body.total),
    body("paid").customSanitizer( value => 0),
  ],
  detail:[
    param("id").notEmpty().customSanitizer(expressValidatorCustom.toMongoId)
  ],
  newPay:[
    body("quotation").notEmpty().customSanitizer(expressValidatorCustom.toMongoId),
    body("date").customSanitizer( value => new Date()),
    body("datepayment").notEmpty().isDate().toDate(),
    body("value").notEmpty().isFloat().toFloat(),
    body("reference").notEmpty().isString(),
  ],
  addService:[
    body("quotation").notEmpty().customSanitizer(expressValidatorCustom.toMongoId),
    body("id").customSanitizer(expressValidatorCustom.newMongoId),
    body("date").customSanitizer( value => new Date()),
    body("name").notEmpty().isString(),
    body("price").notEmpty().isFloat(),
    body("quantity").notEmpty().isFloat(),
    body("total").notEmpty().isFloat(),
  ],
  addInvoice:[
    body("id").customSanitizer(expressValidatorCustom.newMongoId),
    body("quotation").notEmpty().customSanitizer(expressValidatorCustom.toMongoId),
    body("dateCreate").customSanitizer( value => new Date()),
    // body("datexml").notEmpty().isDate().toDate(),
    body("totalxml").notEmpty().isFloat().toFloat(),
    body("comments").notEmpty().isString(),
  ],
  invoiceFile:[
    param("id").notEmpty().customSanitizer(expressValidatorCustom.toMongoId)
  ],
  createQuotationPlan:[
    param("id").notEmpty().customSanitizer(expressValidatorCustom.toMongoId)
  ]
}
module.exports =  { invoiceModel: Model };