'use strict';
const express = require('express');
var router = express.Router();
const { systemController} = require('./system.controller');
const { systemModel } = require('./system.models');
const { expressValidatorCustom } = require('../../helpers/customExpressValidator');

router.get('/modules',systemController.getModules);

module.exports = router;