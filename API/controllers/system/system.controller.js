'use strict'
const { clientDB } = require('../../helpers/mongodb');
const systemController = {
    /**
     * Get all modules of system.
     * @param {*} req 
     * @param {*} res 
     */
    getModules:async (req,res)=>{
        try{
            let response = {code:200, data:""};

            response.data = [
                { home: { access: true }},
                { customers: { access: true }},
            ];

            res.status(response.code).json(response.data);
        }catch(error){
            throw new Error(error);
        }
    },
};

module.exports = { systemController };
