'use strict';
const express = require('express');
var router = express.Router();
const { invoiceController } = require('./quotationplan.controller');
const { invoiceModel } = require('./quotationplan.model');
const { expressValidatorCustom } = require('../../helpers/customExpressValidator');

const multer = require('multer');
const storage = multer.memoryStorage(); // Holds a buffer of the file in memory
const upload = multer({ storage: storage });

router.post('/invoice', invoiceModel.create,expressValidatorCustom.validateRequest,invoiceController.create);
router.get('/invoice',invoiceController.getAll);
router.get('/invoice/:id',invoiceModel.detail,expressValidatorCustom.validateRequest,invoiceController.detDetailByID);

router.post('/quotation/payment',invoiceModel.newPay,expressValidatorCustom.validateRequest,invoiceController.addPay);
router.post('/quotation/service',invoiceModel.addService,expressValidatorCustom.validateRequest,invoiceController.addService);
router.post('/quotation/invoice',upload.fields([{name:"filexml"},{name:"filepdf"}]),invoiceModel.addInvoice,expressValidatorCustom.validateRequest,invoiceController.addInvoice);

router.post('/quotation/plan/:id',invoiceModel.createQuotationPlan,expressValidatorCustom.validateRequest,invoiceController.createPlantAutomatic);

module.exports = router;