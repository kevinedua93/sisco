'use strict'
const { clientDB } = require('../../helpers/mongodb');
const tableName = "quotationplan";
const fs = require('fs');


const Controller = {
  getDetail: async(req,res)=>{
      try {
          let response = {status:"OK", message:null ,data: null};
          let data = await clientDB.db("sisco").collection(tableName).aggregate([
            { $match:{ "_id": req.params.id}},
          ]).toArray();
          response.message = "Get data succesfully";
          res.status(200).json(data);
      } catch (error) {
          throw new Error(error);
      }
  },
};

module.exports = { invoiceController: Controller };
