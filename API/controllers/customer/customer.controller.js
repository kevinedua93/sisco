'use strict'
const { clientDB } = require('../../helpers/mongodb');
const customerController = {
    create: async (req,res)=>{
        try{
            let response = {status:"OK",msg:null ,data: req.body};
            let data = await clientDB.db("sisco").collection("customer").insertOne(req.body);
            response.data = data;
            response.msg = "Create succesfully";
            res.status(200).json(response);
        }catch(error){
            throw new Error(error);
        }
    },
    getAOneCustomerByID: async(req,res)=>{
        try {
            let response = {status:"OK", msg:null ,data: null};
            let data = await clientDB.db("sisco").collection("customer").aggregate([
                { $match:{ "_id": req.params.id}},
                // { $project: {
                //     name:"$name",
                //     type:"$type",
                //     active:"$active"
                // } }
            ]).next();

            response.msg = "Get data succesfully";
            res.status(200).json(data);
        } catch (error) {
            throw new Error(error);
        }
    },
    getAllCustomers: async(req,res)=>{
        try {
            let response = {status:"OK", msg:null ,data: null};
            let data = await clientDB.db("sisco").collection("customer").aggregate([
                {
                    $project:{
                        name:"$name",
                        type:"$type",
                        active:"$active"
                    }
                }
            ]).toArray();
            response.msg = "Get data succesfully";
            res.status(200).json(data);
        } catch (error) {
            throw new Error(error);
        }
    }
};

module.exports = { customerController };
