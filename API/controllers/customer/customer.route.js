'use strict';
const express = require('express');
var router = express.Router();
const { customerController } = require('./customer.controller');
const { customerModel } = require('./customer.model');
const { expressValidatorCustom } = require('../../helpers/customExpressValidator');

router.post('/customer', customerModel.create,expressValidatorCustom.validateRequest,customerController.create);
router.get('/customer',customerController.getAllCustomers);
router.get('/customer/:id',customerModel.detail,expressValidatorCustom.validateRequest,customerController.getAOneCustomerByID);

module.exports = router;