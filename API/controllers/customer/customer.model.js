const {query, body, param} = require('express-validator');
const { expressValidatorCustom } = require('../../helpers/customExpressValidator')

var customerModel = {
    create:[
        //body("employee").notEmpty().isMongoId().customSanitizer(expressValidatorCustom.toMongoId),
        body("name").notEmpty().isString(),
        body("contact.*.name").notEmpty().isString(),
        body("contact.*.email").notEmpty().isEmail(),
        body("contact.*.phone").notEmpty().isString(),
        body("contact.*.email").notEmpty().isString(),
        body("date").customSanitizer( value => new Date()),
        body("active").notEmpty().isBoolean()
    ],
    detail:[
        param("id").notEmpty().customSanitizer(expressValidatorCustom.toMongoId)
    ]
}
module.exports =  { customerModel };