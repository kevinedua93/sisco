'use strict';
const express = require('express');
var router = express.Router();
const { userController } = require('./user.controller');
const { userModel } = require('./user.model');
const { expressValidatorCustom } = require('../../helpers/customExpressValidator');

router.post('/user', userModel.create,expressValidatorCustom.validateRequest,userController.create);
router.get('/user',userController.getAll);

module.exports = router;