'use strict'
const { clientDB } = require('../../helpers/mongodb');
const userController = {
    create: async (req,res)=>{
        try{
            let response = {status:"OK",message:null ,data: req.body};
            let data = await clientDB.db("sisco").collection("user").insertOne(req.body);
            response.data = data;
            response.message = "Create succesfully";
            res.status(200).json(response);
        }catch(error){
            throw new Error(error);
        }
    },
    getAOneByID: async(req,res)=>{
        try {
            let response = {status:"OK", message:null ,data: null};
            let data = await clientDB.db("sisco").collection("user").aggregate([
                { $match:{ "_id": req.params.id}},
                // { $project: {
                //     name:"$name",
                //     type:"$type",
                //     active:"$active"
                // } }
            ]).next();

            response.message = "Get data succesfully";
            res.status(200).json(data);
        } catch (error) {
            throw new Error(error);
        }
    },
    getAll: async(req,res)=>{
        try {
            let response = {status:"OK", message:null ,data: null};
            let data = await clientDB.db("sisco").collection("user").aggregate([
                {
                    $project:{
                        name:"$name",
                        lastname:"$lastname",
                        email:"$email",
                        active:"$active"
                    }
                }
            ]).toArray();
            response.message = "Get data succesfully";
            res.status(200).json(data);
        } catch (error) {
            throw new Error(error);
        }
    }
};

module.exports = { userController };
