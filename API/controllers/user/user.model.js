const {query, body, param} = require('express-validator');
const { expressValidatorCustom } = require('../../helpers/customExpressValidator')

var userModel = {
    create:[
        //body("employee").notEmpty().isMongoId().customSanitizer(expressValidatorCustom.toMongoId),
        body("name").notEmpty().isString(),
        body("lastname").notEmpty().isString(),
        body("email").notEmpty().isString(),
        body("date").customSanitizer( value => new Date()),
        body("active").notEmpty().isBoolean()
    ],
    detail:[
        param("id").notEmpty().customSanitizer(expressValidatorCustom.toMongoId)
    ]
}
module.exports =  { userModel };