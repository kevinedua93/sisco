const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017';
const clientDB = new MongoClient(url);

const mongodbCustom = { 
    connect: async ()=>{
        try {
            await clientDB.connect();
            console.log('[MONGODB] Connected succesfully.');
        } catch (error) {
            throw new Error(error);
        }
    },
};

module.exports = { mongodbCustom, clientDB };