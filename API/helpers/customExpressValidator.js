var { validationResult } = require('express-validator');
const { ObjectId } = require('mongodb');

const expressValidatorCustom = {
    todayDate:(req,res,next)=> new Date(),
    validateRequest:(req,res,next)=>{
        const errors = validationResult(req,{strictParams: true});
        if (!errors.isEmpty()) {
            res.status(400).json({ message:"Need complete all fields required.", errors: errors.array() });
        }else{
            next();
        }
    },
    toMongoId:((value, { req }) => new ObjectId(value) ),
    newMongoId:((value, { req }) => new ObjectId() ),
};

module.exports = { expressValidatorCustom };