'use strict'
var PORT = 3000;
const express = require('express');
var multer = require('multer');
var forms = multer();

// const bodyParser = require('body-parser');
const app = express()
var helmet = require('helmet');
const { mongodbCustom } = require('./helpers/mongodb');
const i18n = require("i18n");
i18n.configure({
  locales: ['en', 'es'],
  defaultLocale:"en",
  directory: './locales',
  objectNotation:true
});

//app.use(express.static(path.join(__dirname, 'public')))
app.use(helmet());
app.use(express.urlencoded({extended: true}));
app.use(express.json());
//app.use(multer().array())
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods","GET,PUT,POST,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Content-Type","application/json");
    next();
  });

app.use(i18n.init)

app.use((req,res,next)=>{
  req.login = { dbname: "dental" };
  //console.trace(req.login);
  next();
});

app.use('/api',require('./controllers/system/system.routes'));
app.use('/api',require('./controllers/customer/customer.route'));
app.use('/api',require('./controllers/user/user.route'));
app.use('/api',require('./controllers/invoice/invoice.route'));
// app.use('/api',require('./controllers/employee/employees.routes'));
// app.use('/api',require('./controllers/checker/checker.routes'));

app.use((err, req, res, next) => {
  console.error(err)
  let response = {
    message:"Was ocurred error in server.",
    error : err
  }
  res.status(500).json(response);
})

mongodbCustom.connect().then( res => {
  app.listen(PORT,()=>{ console.log("[SERVER] Start server on "+ PORT+" succesfully");  });
}).catch(error =>{
  console.log(error);
});